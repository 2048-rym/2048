README
======

0. git clone https://gitorious.org/2048-rym/2048.git
1. npm install
2. chmod 775 abcd.js # rendre exécutable
2. ./abcd.js # lancer le jeu interactif

Pour jouer, utiliser les touches vim h, j, k et l pour gauche, bas, haut et droite respectivement pour chaque déplacement.

TODO
=====
* Faire fonctionner avec des dimensions autres que 4x4
* Faire fonctionner avec des dimensions autres qu'un carré
