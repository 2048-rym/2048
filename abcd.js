#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"
// explication: http://unix.stackexchange.com/questions/65235/universal-node-js-shebang

;(function() {
    "use strict";

    var keypress = require('keypress');
    var board = [], empty = ' ', cmds = {
        h: left,
        j: down,
        k: up,
        l: right
    };

    function init(h, w) {
        var c, l;

        h = h || 4;
        w = w || 4;

        for (l = 0; l < h; ++l) {
            board.push([]);
            board[l] = '';
            for (c = 0; c < w; ++c) {
                board[l] += empty;
            }
        }

        number();
        number();
    }

    function full() {
        var l, c, dirty = true;
        for (l = 0; l < board.length; ++l) {
            for (c = 0; c < board[l].length; ++c) {
                dirty = dirty && (empty !== board[l][c]);
            }
        }
        return dirty;
    }

    function number(y, x) {
        var l, c, z;

        // game over
        if (full()) {
            return true;
        }

        if (('undefined' !== typeof y) && ('undefined' !== typeof x)) {
            l = y;
            c = x;
        } else {
            do {
                l = Math.floor(Math.random()*board.length);
                c = Math.floor(Math.random()*board[0].length);
            } while (empty !== board[l][c]);

        }
        z = board[l].split('');
        // 10% de '4' selon http://www.reddit.com/r/2048/comments/23tzxu/odds_of_a_4_spawning/
        z[c] = Math.random() < 0.9 ? 'a' : 'b';
        board[l] = z.join('');
    }

    function show() {
        var l;
        for (l = 0; l < board.length; ++l) {
            console.log('* ' + board[l] + ' *');
        }
        console.log('\n');
    }

    function line(l) {
        return board[l];
    }

    function col(c) {
        var l, ret = '';

        for (l = 0; l < board.length; ++l) {
            ret += board[l][c];
        }
        return ret;
    }

    function padRight(str, n) {
        var r, p = '';

        // devrait être détecté automatiquement pour supporter différentes dimensions
        n = n || 4;

        for (r = 0; r < n; ++r) {
            p = p + empty;
        }

        return (p + str).slice(-n);

    }

    function rotate90() {
        var z, l, c, board2 = [];

        for (l = 0; l < board.length; ++l) {
            board2.push([]);
            for (c = 0; c < board[l].length; ++c) {
                board2[l].push('');
            }
        }

        for (l = 0; l < board.length; ++l) {
            for (c = 0; c < board[l].length; ++c) {
                board2[c][l] = board[3-l][c];
            }
        }

        for (l = 0; l < board.length; ++l) {
            board[l] = board2[l].join('');
        }
    }

    function rotate270() {
        rotate90();
        rotate90();
        rotate90();
    }

    function rotate180() {
        rotate90();
        rotate90();
    }

    function up() {
        var moved;
        console.log('move up');
        rotate90();
        moved = right(true);
        rotate270();
        return moved;
    }

    function down() {
        var moved;
        console.log('move down');
        rotate270();
        moved = right(true);
        rotate90();
        return moved;
    }

    function left() {
        var moved;
        console.log('move left');
        rotate180();
        moved = right(true);
        rotate180();
        return moved;
    }

    function right(q) {
        var rx, r, l, c, line, dirty, z, moved = false, orig;

        if (!q) {
            console.log('move right');
        }

        for (l = 0; l < board.length; ++l) {
            line = ''; dirty = false;
            for (c = 0; c < board[0].length; ++c) {
                dirty = dirty || (empty !== board[l][c]);
                line += board[l][c];
            }

            if (dirty) {
                orig = board[l];
                z = line.split('').reverse().join('');
                z = z.replace(/ +/g, '');

                // aa -> b, bb -> c, cc -> d...
                for (r = 'y'; r >= 'a'; r = String.fromCharCode(r.charCodeAt()-1)) {
                    rx = new RegExp(r + r, 'g');
                    z = z.replace(rx, String.fromCharCode(1+r.charCodeAt())); // r + r au lieu de rx
                };
                line = z.split('').reverse().join('');
                board[l] = padRight(line); // , board[0].length
                moved = moved || (orig !== board[l]);
            }
        }
        return moved;
    }

    // make `process.stdin` begin emitting "keypress" events
    keypress(process.stdin);

    process.stdin.on('keypress', function (ch, key) {
        var cmd;
        if (key && key.ctrl && key.name == 'c') {
            process.stdin.pause();
        }

        cmd = cmds[key.name];
        if (cmd) {
            if (cmd()) {
                if (!number()) {
                    show();
                }
            } else {
                show();
            }
        }
    });

    // permet de faire par exemple:
    // echo hjhjhjlhjhjl|./abcd.js
    if (process.stdin.setRawMode) {
        process.stdin.setRawMode(true);
        process.stdin.resume();
    }

//    init(5, 5);
    init();
    show();
}());
